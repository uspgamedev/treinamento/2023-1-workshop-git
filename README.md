# Workshop de Git Básico

Este repositório contém o material usado para ministrar o workshop de Git
Básico como parte da turma de treinamento 2023-1 do USPGameDev.

## Instalando Git

Em linux: use o gerenciador de pacotes do sistema, por exemplo:

```bash
$ sudo apt install git
```

Em windows: [baixe][1] e instale.

[1]: https://git-scm.com/download/win

## Referência rápida dos comandos mais comuns

Mais ou menos na ordem em que usamos no dia-a-dia.

> `git init`

Cria um repositório novo na pasta atual. Só precisa fazer a primeira vez que o
repositório é **criado**.

> `git clone <endereço>`

Clona um repositório e adiciona `<endereço>` como a referência remota "origin".
Só precisa fazer a primeira vez que você **abaixa um repositório já existente**
no seu computador.

> `git pull [--rebase]`

Faz download dos commits mais recentes e adiciona no seu ramo "main" local.
Minha sugestão é sempre usar `--rebase` para não gerar commits de merge
desnecessariamente, o que também evita de ter que abrir o editor de texto
para confirmar a mensagem do commit de merge.

> `git status`

Mostra as mudanças detectadas pelo Git no repositório local com relação ao
último commit.

> `git add <arquivo>`

Prepara o arquivo para entrar no próximo commit. Você pode escrever uma vez
para cada arquivo ou listar todos os arquivos juntos, separados por espaço.
Também é possível adicionar pastas e o Git incluíra tudo que tiver dentro
delas.

> `git commit`

Faz um commit com os arquivos preparados (*staged*). Isso vai abrir o editor
de texto padrão do Git para você escrever uma mensagem. Normalmente, essas
mensagens seguem o formato:

```
Título: o que eu fiz nesse commit

Corpo: explicação do **porquê** eu fiz essas mudanças,
para que outros possam consultar no futuro e entender
o contexto do commit.
```

[Mais informações sobre como escrever bons commits][2].

[2]: https://cbea.ms/git-commit/

> `git log`

Lista o histórico de commits.

> `git push`

Faz upload dos commits locais que ainda não foram adicionados ao repositório
remoto. Se falhar, provavelmente é porque você precisa fazer um `pull` antes.

## Notes

![](notes/1-intro.svg)
![](notes/2-about-git.svg)
![](notes/3-making-commits.svg)
![](notes/4-remote-branches.svg)
